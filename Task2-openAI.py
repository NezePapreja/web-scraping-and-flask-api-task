import openai
import os
from flask import Flask, request, jsonify

app = Flask(__name__)

openai.api_key = os.getenv('OPENAI_API_KEY')

def generate_sales_data(company, year, country):
    prompt = f"Generate a sales report for {company} in {country} for the year {year}. Provide sales figures for each month."
    
    response = openai.Completion.create(
        model="davinci-002",  
        prompt=prompt,
        max_tokens=150
    )
    
    sales_data_text = response['choices'][0]['text'].strip()
    sales_data_lines = sales_data_text.split("\n")
    
    sales_data = {}
    for line in sales_data_lines:
        month, sales = line.split(": ")
        sales_data[month] = int(sales.replace(",", ""))
    
    return sales_data

@app.route('/')
def home():
    return "Welcome to the Sales Report API!"

@app.route('/generate_report', methods=['POST'])
def generate_report():
    data = request.json
    company = data.get('company')
    year = data.get('year')
    country = data.get('country')
    
    if not (2000 <= year <= 2024):  
        return jsonify({"error": "Year out of range"}), 400
    
    sales_data = generate_sales_data(company, year, country)
    
    response = {
        "company": company,
        "year": year,
        "country": country,
        "sales_data": sales_data
    }
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)
