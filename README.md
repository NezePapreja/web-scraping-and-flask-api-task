# Web Scraping and Flask API task



## Task 1: Web Scraping News Articles from Yahoo Finance

Objective:
Scrape the latest economic news articles from Yahoo Finance.
URL to Scrape:
`https://finance.yahoo.com/topic/economic-news/`

Practice:
`https://quotes.toscrape.com/tag/books/`

## Task 2: Create a Flask API to Generate a Sales Report for a Mobile Company Using Generative AI

Objective: Create an API using Flask that:
1. Accepts requests for generating sales reports.
2. Utilizes a generative AI model to simulate or generate the sales data.
3. Provides sales data for a specific mobile company, year, and country based on the request
parameters.
Requirements:
1. API Parameters:
o Company: The name of the mobile company.
o Year: The year for which the sales report is requested.
o Country: The country for which the sales report is relevant.
2. Validation:
o Ensure the year is within a reasonable range (e.g., between 2000 and the current year).

