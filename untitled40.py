import torch
from PyPDF2 import PdfReader
from transformers import DPRQuestionEncoder, DPRQuestionEncoderTokenizer
from transformers import DPRContextEncoder, DPRContextEncoderTokenizer
from transformers import BartForConditionalGeneration, BartTokenizer

question_tokenizer = DPRQuestionEncoderTokenizer.from_pretrained('facebook/dpr-question_encoder-single-nq-base')
question_encoder = DPRQuestionEncoder.from_pretrained('facebook/dpr-question_encoder-single-nq-base')

context_tokenizer = DPRContextEncoderTokenizer.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
context_encoder = DPRContextEncoder.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')

generator_tokenizer = BartTokenizer.from_pretrained('facebook/bart-large')
generator_model = BartForConditionalGeneration.from_pretrained('facebook/bart-large')

def extract_text_from_pdf(pdf_path):
    reader = PdfReader(pdf_path)
    text = ""
    for page in reader.pages:
        text += page.extract_text() + "\n"
    return text

pdf_path = "your_pdf_file.pdf"
pdf_text = extract_text_from_pdf(pdf_path)
context_texts = pdf_text.split("\n\n")

question = "What is retrieval-augmented generation?"

question_inputs = question_tokenizer(question, return_tensors='pt')
question_embeds = question_encoder(**question_inputs).pooler_output

retrieved_docs = []
for idx, context_text in enumerate(context_texts):
    context_inputs = context_tokenizer(context_text, return_tensors='pt', truncation=True)
    context_embeds = context_encoder(**context_inputs).pooler_output
    similarity_score = torch.matmul(question_embeds, context_embeds.T).item()
    retrieved_docs.append((similarity_score, context_text))

retrieved_docs = sorted(retrieved_docs, key=lambda x: x[0], reverse=True)
top_docs = [doc for _, doc in retrieved_docs[:3]]

input_text = question + " " + " ".join(top_docs)
inputs = generator_tokenizer(input_text, return_tensors='pt', truncation=True)
outputs = generator_model.generate(**inputs)

generated_text = generator_tokenizer.decode(outputs[0], skip_special_tokens=True)
print("Generated Answer:", generated_text)
