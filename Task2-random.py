from flask import Flask, request, jsonify
import random

app = Flask(__name__)

def generate_sales_data(year, country):
    sales_data = {month: random.randint(1000, 5000) for month in 
        ["January", "February", "March", "April", "May", "June", 
         "July", "August", "September", "October", "November", "December"]}
    return sales_data

@app.route('/')
def home():
    return "Welcome to the Sales Report API!"

@app.route('/generate_report', methods=['POST'])
def generate_report():
    data = request.json
    company = data.get('company')
    year = data.get('year')
    country = data.get('country')
    
    if not (2000 <= year <= 2024):  
        return jsonify({"error": "Year out of range"}), 400
    
    sales_data = generate_sales_data(year, country)
    
    response = {
        "company": company,
        "year": year,
        "country": country,
        "sales_data": sales_data
    }
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)
