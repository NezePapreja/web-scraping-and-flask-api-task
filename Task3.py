import openai
from flask import Flask, request, jsonify

app = Flask(__name__)

# Set your OpenAI API key here
openai.api_key = 'sk-proj-fUSIGl1usRj4juftahOkT3BlbkFJFFqkamGqGWAPAViBCC1N'

@app.route('/')
def home():
    return "Welcome to the Addition API!"

@app.route('/add_numbers', methods=['POST'])
def add_numbers():
    data = request.json
    num1 = data.get('num1')
    num2 = data.get('num2')

    # Ensure both numbers are provided
    if num1 is None or num2 is None:
        return jsonify({"error": "Please provide both numbers"}), 400

    # Convert numbers to integers and perform addition
    try:
        num1 = int(num1)
        num2 = int(num2)
        # Create prompt for OpenAI
        prompt = f"Add {num1} and {num2}."
        
        # Call OpenAI API to perform addition
        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "user", "content": prompt}]
        )
        
        # Extract the result from OpenAI response
        result = response['choices'][0]['message']['content'].strip()
        sum_result = int(result)  # Convert the result to an integer
        
        # Return the result as JSON
        return jsonify({
            "num1": num1,
            "num2": num2,
            "sum": sum_result
        })
    
    except ValueError:
        return jsonify({"error": "Invalid input, please provide valid numbers"}), 400

if __name__ == '__main__':
    app.run(debug=True)
