def load_document(file_path):
    with open(file_path, 'r') as file:
        content = file.read()
    return content

from langchain.chains import SimpleChain
from langchain.llms import OpenAI

def define_chain():
    llm = OpenAI(api_key='key')
    
    def summarize_text(text):
        return llm({"prompt": f"Summarize the following text:\n\n{text}", "max_tokens": 150})
    
    return SimpleChain(summarize_text)
def main(file_path):
    document_content = load_document(file_path)
    chain = define_chain()
    output = chain.run(document_content)
    print("Summary:")
    print(output)
file_path = 'testing1.txt'
main(file_path)

